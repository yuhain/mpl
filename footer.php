<footer class="page-footer font-small stylish-color-dark bg-dark mt-5 text-white">
  <div class="container text-center text-md-left">
    <div class="row">
      <div class="col-md-3 mx-auto">
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Malaysian Public Library</h5>
        <p>Malaysian Public Library(MPL) is a online website that allows people to gain access to various journals, articles and more online</p>
      </div>

      <hr class="clearfix w-100 d-md-none">

      <div class="col-md-2 mx-auto">
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Resources</h5>
        <ul class="list-unstyled">
          <li>
            <a href="#!">OPAC</a>
          </li>
          <li>
            <a href="onlineDatabase.php">Online Databases</a>
          </li>
          <li>
            <a href="onlineJornal.php">Online Journals</a>
          </li>
          <li>
            <a href="ebook.php">e-Books</a>
          </li>
          <li>
            <a href="#!">Multimedia Resources</a>
          </li>
          <li>
            <a href="enewspaper.php">Old e-Newspapers</a>
          </li>
        </ul>
      </div>
      <div class="col-md-2 mx-auto">
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Events</h5>
        <ul class="list-unstyled">
          <li>
            <a href="#!">Cultural Activities</a>
          </li>
          <li>
            <a href="#!">Exhibition</a>
          </li>
          <li>
            <a href="#!">Awards/Compeitions</a>
          </li>
          <li>
            <a href="#!">Reading Activities</a>
          </li>
          <li>
            <a href="#!">Talks/Workshops</a>
          </li>
        </ul>
      </div>

      <hr class="clearfix w-100 d-md-none">

      <div class="col-md-2 mx-auto">
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Links</h5>
        <ul class="list-unstyled">
          <li>
            <a href="#!">Sponsorship</a>
          </li>
          <li>
            <a href="#!">Career</a>
          </li>
          <li>
            <a href="membership.php">Membership</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3 mx-auto" >
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4"><a class="text-danger" href="contactUs.php">Contact Us</a></h5>
        <address >
          Address: <br>
          2nd floor, Wisma HELP, Jalan Dungun
          50490 Kuala Lumpur, Malaysia <br>
          Tel:&nbsp;<a href="tel:603-2711-2000">603-2711 2000</a> <br>
          Email:&nbsp; <a type="email" href= "mailto:yuhain1999@gmail.com">MPLRDC</a>
        </address>
      </div>
    </div>
  </div>

  <div class="footer-copyright text-center py-3">
    © 2020 Copyright: HELP University
  </div>
</footer>
