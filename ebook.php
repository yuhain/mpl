<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Anton">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <title>Malaysian Public Library</title>
  </head>
  <body class="eb-bg">
    <?php include 'navigation.php'; ?>
    <div class="container-fluid en" style="height: 640px;">
    <h1 class="display-4 text-center mb-5 text-white ">E-Books</h1>
    <br>
    <br>
    <br>
    <br>
    <h2 class="text-center display-3 mb-5 text-white">English E-Books Coming Soon!</h2>
    <h3 class="text-center display-4 mb-5 text-white">Head over and click on Chinese up top to view the Chinese E-Books!</h2>
    </div>
    <div class="container-fluid chi" style="height: 640px;">
      <h1 class="display-4 text-center mb-5 text-white ">电子书</h1>


      <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-interval="false">
        <ol class="carousel-indicators" style="bottom: -70px;">
          <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active">D-F</li>
          <li data-target="#carouselExampleCaptions" data-slide-to="1">J-Q</li>
          <li data-target="#carouselExampleCaptions" data-slide-to="2">S-Y</li>
        </ol>
        <div class="carousel-inner">
          <div class="container-sm">
            <div class="carousel-item active">
              <div class="row">
                <div class="col-md ml-1 mr-1" style="height: 450px;">
                  <div class="card bg-light" style="height: 348px;" data-aos="zoom-in">
                    <h5 class="card-header text-center text-body">D</h5>
                    <div class="card-body">

                      <div class="dropdown mb-3">
                        <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        都市言情小说
                        </button>
                        <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                          <div class="col-md">
                            <hr><a class="dropdown-item" href="https://www.jjwxc.net/" target="_blank"><img src="Image\onlineDatabese\logo_2x.png" href="https://www.google.com.my/" class="rounded mx-auto d-block w-50" alt="..."></a><hr>
                          </div>
                        </div>
                      </div>

                      <div class="dropdown">
                        <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        咚漫
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <div class="col-md">
                            <hr><a class="dropdown-item" href="https://m.dongmanmanhua.cn/" target="_blank"><img src="Image\onlineDatabese\tablet_banner_top.jpg" class="rounded mx-auto d-block w-100" alt="..."></a><hr>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md ml-1 mr-1" style="height: 450px;">
                  <div class="card bg-light" style="height: 348px;" data-aos="zoom-in">
                    <h5 class="card-header text-center text-body">E</h5>
                    <div class="card-body">

                      <div class="dropdown mb-3">
                        <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        epubee
                        </button>
                        <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                          <div class="col-md">
                            <hr><a class="dropdown-item" href="http://cn.epubee.com/books/" target="_blank"><img src="Image\onlineDatabese\tablet_banner_top.jpg" class="rounded mx-auto d-block w-100" alt="..."></a><hr>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md ml-1 mr-1" style="height: 450px;">
                  <div class="card bg-light" style="height: 348px;" data-aos="zoom-in">
                    <h5 class="card-header text-center text-body">F</h5>
                    <div class="card-body">

                        <div class="dropdown mb-3">
                          <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          番茄小说网
                          </button>
                          <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                            <div class="col-md">
                              <hr><a class="dropdown-item" href="http://www.fqxsw.cc/" target="_blank"><img src="Image\onlineDatabese\cern-logo-large.png" class="rounded mx-auto d-block w-25" alt="..."></a><hr>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="carousel-item">
              <div class="row">
                <div class="col-md ml-1 mr-1" style="height: 450px;">
                  <div class="card bg-light" style="height: 348px;">
                    <h5 class="card-header text-center text-body">J</h5>
                    <div class="card-body">

                      <div class="dropdown mb-3">
                        <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        久久小说网
                        </button>
                        <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                          <div class="col-md">
                            <hr><a class="dropdown-item" href="https://m.jjxsw.la/" target="_blank"><img src="Image\onlineDatabese\dart-logo-transparent.gif" class="rounded mx-auto d-block w-50" alt="..."></a><hr>
                          </div>
                        </div>
                      </div>

                      <div class="dropdown mb-3">
                        <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        精品文学网
                        </button>
                        <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                          <div class="col-md">
                            <hr><a class="dropdown-item" href="http://www.bestory.com/" target="_blank"><img src="Image\onlineDatabese\DCsunburst.png" class="rounded mx-auto d-block" alt="..."></a><hr>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md ml-1 mr-1" style="height: 450px;">
                  <div class="card bg-light" style="height: 348px;">
                    <h5 class="card-header text-center text-body">M</h5>
                    <div class="card-body">

                      <div class="dropdown mb-3">
                        <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        m漫画岛
                        </button>
                        <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                          <div class="col-md">
                            <hr><a class="dropdown-item" href="https://m.manhuadao.cn/" target="_blank"><img src="Image\onlineDatabese\eric_large.png" class="rounded mx-auto d-block w-75" alt="..."></a><hr>
                          </div>
                        </div>
                      </div>

                      <div class="dropdown mb-3">
                        <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        猫眼看书小说网
                        </button>
                        <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                          <div class="col-md">
                            <hr><a class="dropdown-item" href="https://m.maoyankanshu.com/" target="_blank"><img src="Image\onlineDatabese\bl_logo_100.jpg" class="rounded mx-auto d-block" alt="..."></a><hr>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md ml-1 mr-1" style="height: 450px;">
                  <div class="card bg-light" style="height: 348px;">
                    <h5 class="card-header text-center text-body">Q</h5>
                    <div class="card-body">

                      <div class="dropdown mb-3">
                        <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        奇妙漫画
                        </button>
                        <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                          <div class="col-md">
                            <hr><a class="dropdown-item" href="https://m.qimiaomh.com/" target="_blank"><img src="Image\onlineDatabese\google_book.png" class="rounded mx-auto d-block w-75" alt="..."></a><hr>
                          </div>
                        </div>
                      </div>

                      <div class="dropdown mb-3">
                        <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        全本小说网
                        </button>
                        <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                          <div class="col-md">
                            <hr><a class="dropdown-item" href="http://quanben-xiaoshuo.com/" target="_blank"><img src="Image\onlineDatabese\scholar_logo_64dp.png" class="rounded mx-auto d-block w-100" alt="..."></a><hr>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="carousel-item">
              <div class="row">
                <div class="col-md ml-1 mr-1" style="height: 450px;">
                  <div class="card bg-light" style="height: 348px;">
                    <h5 class="card-header text-center text-body">S</h5>
                    <div class="card-body">

                      <div class="dropdown mb-3">
                        <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        360 言情小说
                        </button>
                        <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                          <div class="col-md">
                            <hr><a class="dropdown-item" href="http://www.yanqing360.com/" target="_blank"><img src="Image\onlineDatabese\hku-logo.jpg" class="rounded mx-auto d-block w-100" alt="..."></a><hr>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md ml-1 mr-1" style="height: 450px;">
                  <div class="card bg-light" style="height: 348px;">
                    <h5 class="card-header text-center text-body">T</h5>
                    <div class="card-body">

                      <div class="dropdown mb-3">
                        <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        台湾言情
                        </button>
                        <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                          <div class="col-md">
                            <hr><a class="dropdown-item" href="http://www.yue263.com/taiwanyq/" target="_blank"><img src="Image\onlineDatabese\ideas4.jpg" class="rounded mx-auto d-block w-50" alt="..."></a><hr>
                          </div>
                        </div>
                      </div>

                      <div class="dropdown mb-3">
                        <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        腾讯动漫
                        </button>
                        <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                          <div class="col-md">
                            <hr><a class="dropdown-item" href="https://m.ac.qq.com/" target="_blank"><img src="Image\onlineDatabese\download.png" class="rounded mx-auto d-block w-75" alt="..."></a><hr>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md ml-1 mr-1" style="height: 450px;">
                  <div class="card bg-light" style="height: 348px;">
                    <h5 class="card-header text-center text-body">Y</h5>
                    <div class="card-body">

                        <div class="dropdown mb-3">
                          <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          言情小说库
                          </button>
                          <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                            <div class="col-md">
                              <hr><a class="dropdown-item" href="https://www.yqxsk.net/didian/3/" target="_blank"><img src="Image\onlineDatabese\logo-l.png" class="rounded mx-auto d-block w-100" alt="..."></a><hr>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>

    <?php include 'footer.php'; ?>





    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
     AOS.init({
        duration: 2000
      });
    </script>
    <script src="js/language.js"></script>
  </body>
</html>
