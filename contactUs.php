<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Anton">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <title>Contact Us</title>
  </head>
  <body>

    <?php include 'navigation.php'; ?>
  <!-- english -->
    <div class="contact-bg">
      <div class="container-sm d-sm-flex align-items-center" style="height: 160%;">
        <div class="row" style="width: 150%;">
          <div class="col-md lead">
            <div class="row en">
              <div class="col-sm d-flex justify-content-center">
                <div class="card mb-3" style=" width: 22rem;">
                  <div class="card-header">Operating Hours</div>
                  <div class="card-body shadow bg-white p-3 rounded">
                    <h5 class="card-title lead"><strong>Opening Hour</strong></h5>
                    <p class="text-sm-left" style="width: 80%;">Tue – Fri &nbsp;  : &nbsp; &nbsp; &nbsp;  9am – 4pm</p>
                    <p class="text-sm-left" style="width: 80%;">Sat – Sun &nbsp;:&nbsp;&nbsp; &nbsp; 12pm – 4pm  </p>
                    <h5 class="card-title lead"><strong>Closed</strong></h5>
                    <p class="text-sm-left">Mondays & Public Holidays</p>
                  </div>
                </div>
              </div>
            </div>
<!-- chinese -->
            <div class="row chi">
              <div class="col-sm d-flex justify-content-center">
                <div class="card mb-3" style=" width: 22rem;">
                  <div class="card-header">图书馆开关时间</div>
                  <div class="card-body shadow bg-white p-3 rounded">
                    <h5 class="card-title lead"><strong>开放时间</strong></h5>
                    <p class="text-sm-left" style="width: 80%;">星期二至星期五 &nbsp; 9am – 4pm</p>
                    <p class="text-sm-left" style="width: 80%;">星期六至星期日 12pm – 4pm </p>
                    <h5 class="card-title lead"><strong>休息</strong></h5>
                    <p class="text-sm-left">星期一及公共假期</p>
                  </div>
                </div>
              </div>
            </div>
<!-- address english -->
            <div class="row en">
              <div class="col-sm d-flex justify-content-center">
                <div class="card mb-3" style=" width: 22rem;">
                  <div class="card-header">Contact</div>
                  <div class="card-body shadow bg-white p-3 rounded">
                    <h5 class="card-title lead"><strong>Address</strong></h5>
                    <address class="text-justify">
                      2nd floor, Wisma HELP, Jalan Dungun <br>
                      50490 Kuala Lumpur, Malaysia <br>
                      Tell:&nbsp; &nbsp; &nbsp;<a href="tel:603-2711-2000">603-2711 2000</a> <br>
                      Email:&nbsp; <a type="email" href= "mailto:yuhain1999@gmail.com">MPLRDC</a>
                    </address>
                  </div>
                </div>
              </div>
            </div>
<!-- address chinese -->
            <div class="row chi">
              <div class="col-sm d-flex justify-content-center">
                <div class="card mb-3" style=" width: 22rem;">
                  <div class="card-header">联系</div>
                  <div class="card-body shadow bg-white p-3 rounded">
                    <h5 class="card-title lead"><strong>联系我们</strong></h5>
                    <address class="text-justify">
                      2nd floor, Wisma HELP, Jalan Dungun <br>
                      50490 Kuala Lumpur, Malaysia <br>
                      Tell:&nbsp; &nbsp; &nbsp;<a href="tel:603-2711-2000">603-2711 2000</a> <br>
                      Email:&nbsp; <a type="email" href= "mailto:yuhain1999@gmail.com">MPLRDC</a>
                    </address>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm">
            <div class="embed-responsive embed-responsive-1by1">
              <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15935.117839156466!2d101.6652778!3d3.1527778!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1eb08ed1feb2398d!2sWisma%20HELP!5e0!3m2!1sen!2smy!4v1596256274154!5m2!1sen!2smy" allowfullscreen></iframe>
            </div>
          </div>
        </div>
      </div>
      <?php include 'footer.php'; ?>
    </div>







    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <script>
      AOS.init({
        duration: 2000
      });
    </script>
    <script src="js/language.js"></script>
  </body>
</html>
