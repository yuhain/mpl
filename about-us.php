<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Anton">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <title>Malaysian Public Library</title>

  </head>

  <!-- style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url('bg-introduction.jpg'); height: 100%; background-repeat: no-repeat; background-size: cover; background-blend-mode:luminosity;" -->
  <body>

    <?php include 'navigation.php'; ?>

    <div class="container-sm">
      <h1 class="display-4 text-center mt-5 mb-5">Introduction</h1>
      <div class="col-sm shadow p-3 bg-white rounded" style="margin-bottom: 100px;"  data-aos="fade-up">
        <div class="row">
          <div class="col-sm">
            <img src="Image/Introduction/introduction-img1.jpg" class="img-fluid" alt="Responsive image">
          </div>
          <div class="col-sm">
            <p class="lead text-justify" style="font-size: 22.4px;">
              The Malaysian Public Library, Research and Development Centre
              (formerly known as the Malayan Public Library Association)
              was formed in 1955. Since then, this historical library has made
              immense contributions to the mental and intellectual development
              of Malaysians, both in the urban and non-urban areas. It has had
              a chequered history of providing reading materials and information
              services for the upliftment of the young and old through times of
              trouble and social instability.</p>
          </div>
        </div>
      </div>
    </div>

    <div class="jumbotron jumbotron-fluid  text-white" style="background: #222">
      <div class="container">
        <div class="row">
          <div class="col-sm my-auto" data-aos="flip-left">
            <h1 class="display-4">Location</h1>
            <p class="lead">Presently it is located on the 2nd floor, Wisma HELP, Jalan Dungun 50490 Kuala Lumpur.</p>
          </div>
          <div class="col-sm" data-aos="flip-right">
            <div id="map-container-google-2" class="embed-responsive embed-responsive-16by9" style="height: 450px">
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15935.117839156466!2d101.6652778!3d3.1527778!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1eb08ed1feb2398d!2sWisma%20HELP!5e0!3m2!1sen!2smy!4v1596256274154!5m2!1sen!2smy"
              width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="jumbotron jumbotron-fluid bg-white">
      <div class="row w-100">
        <div class="col-sm-6 shadow p-3 bg-white rounded ml-5" data-aos="zoom-in">
          <img src="Image/Introduction/H_gallery_20.jpg" class="img-fluid" alt="Responsive image">
        </div>
        <div class="col-sm-5">
          <p class="text-justify mt-2 lead" style="font-size: 17.8px;" data-aos="zoom-out">The library, a non-profit
          organization, is managed by a Council of Board Members.
          The financial support comes mainly from members’ subscription
          fees and donations from the public. In 2002, HELP University College
          was invited to join the Council and to provide financial support
          as well as manpower to manage the library. <br><br>

          Currently, it has a collection of over 60,000 books, mostly in Chinese.
          The subjects cover a wide range from classical and modern literature to
          current topics of interest. Most of the non-Chinese books were donated
          by the library members and the Chinese community.<br><br>

          The management would like to encourage the Chinese community, students
          and other interested members of the public to come and enjoy the library’s
          full range of facilities as an important complement to their formal and
          informal education.</p>
        </div>
      </div>
    </div>

    <div class="jumbotron jumbotron-fluid text-white" style="background: #222">
      <div class="container">
        <h1 class="display-4 text-center"><hr class="bg-secondary">Facilities and Future<hr class="bg-secondary"></span></h1>
        <p class="lead text-justify">The management plans to upgrade the library to help promote a strong reading culture,
          enhance educational standards, upgrade general knowledge and strengthen relationships among the
          various communities in Malaysia. The plans include:</p>
      </div>
      <div class="container-sm text-white lead te">
        <div class="row text-center mb-3">
          <div class="col-sm mr-3" data-aos="flip-right">
            <img src="Image/Introduction/seminar.jpg" class="rounded d-block w-50 mx-auto shadow p-3 bg-white rounded " alt="...">
            <p><br>Organising seminars and talks on important topics</p>
          </div>
          <div class="col-sm" data-aos="flip-right" data-aos-delay="500">
            <img src="Image/Introduction/computerizedCatalogue.png" class="rounded mx-auto d-block w-50  shadow p-3 bg-white rounded " alt="...">
            <p><br>Providing a computerized catalogue service to facilitate quick access to books and information</p>
          </div>
          <div class="col-sm mr-3" data-aos="flip-right" data-aos-delay="1000">
            <img src="Image/Introduction/LibraryCollection.jpg" class="rounded mx-auto d-block w-50  shadow p-3 bg-white rounded " alt="...">
            <p><br>Increasing the variety and size of the library collection</p>
          </div>
        </div>


        <div class="row text-center justify-content-md-center">
          <div class="col-sm-4 mr-3" data-aos="flip-right" data-aos-delay="1500">
            <img src="Image/Introduction/computer.jpg" class="rounded mx-auto d-block w-50  shadow p-3 bg-white rounded " alt="...">
            <p><br>Offering computer and internet access</p>
          </div>
          <div class="col-sm-4" data-aos="flip-right" data-aos-delay="2000">
            <img src="Image/Introduction/readingArea.jpg" class="rounded mx-auto d-block w-50  shadow p-3 bg-white rounded " alt="...">
            <p><br>Opening more reading areas</p>
          </div>
        </div>
      </div>
    </div>

    <?php include 'footer.php'; ?>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <script>
     AOS.init({
        duration: 2000
      });
    </script>
<script src="js/language.js"></script>
  </body>
</html>
