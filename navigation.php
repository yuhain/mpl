<!-- NavBar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top" id="eng_nav">
      <!-- Title Website -->
      <a class="navbar-brand" href="index.php"><h1 style="color:#F5502D; font-family:Anton;">MPL</h1></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Navigation -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">

        <!-- About Us -->
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              About Us
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown"> <!-- Dropdown option -->
              <a class="dropdown-item" href="about-us.php">Introduction</a>
              <a class="dropdown-item" href="#">Publications</a>
            </div>
          </li>

          <!-- OPAC -->
          <li class="nav-item">
            <a class="nav-link" href="#">OPAC<span class="sr-only">(current)</span></a>
          </li>

          <!-- Research -->
          <li class="nav-item">
            <a class="nav-link" href="#">Research</a>
          </li>

          <!-- Resources -->
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Resources
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown"> <!-- Dropdown option -->
              <a class="dropdown-item" href="onlineDatabase.php">Online Databases</a>
              <a class="dropdown-item" href="onlineJournal.php">Online Journals</a>
              <a class="dropdown-item" href="ebook.php">e-Books</a>
              <a class="dropdown-item" href="#">Multimedia Resources</a>
              <a class="dropdown-item" href="enewspapers.php">Old e-Newspapers </a>
            </div>
          </li>

          <!-- Events -->
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Events
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown"> <!-- Dropdown option -->
              <a class="dropdown-item" href="#">Cultural Activities</a>
              <a class="dropdown-item" href="#">Exhibitions</a>
              <a class="dropdown-item" href="#">Awards/Competitions</a>
              <a class="dropdown-item" href="#">Reading Activities</a>
              <a class="dropdown-item" href="#">Talks/Workshops</a>
            </div>
          </li>

          <!-- Sponsorship -->
          <li class="nav-item">
            <a class="nav-link" href="#">Sponsorship</a>
          </li>

          <!-- Careers -->
          <li class="nav-item">
            <a class="nav-link" href="#">Career</a>
          </li>

          <!-- Membership -->
          <li class="nav-item">
            <a class="nav-link" href="membership.php">Membership</a>
          </li>

          <!-- Contact Us -->
          <li class="nav-item">
            <a class="nav-link" href="contactUs.php">Contact Us</a>
          </li>
        </ul>
        <div>
          <button id="en_click" type="button" class="btn btn-danger">English</button>
          <h7 style="color: white; font-size: 20px;">/</h7>
          <button id="chi_click" type="button" class="btn btn-danger mr-3">Chinese</button>
        </div>

        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Search</button>
        </form>
      </div>

    </nav>

    <!-- chinese -->
    <!-- NavBar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="chinese_nav">
      <!-- Title Website -->
      <a class="navbar-brand" href="index.php"><h1 style="color:#F5502D; font-family:Anton;">MPL</h1></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Navigation -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          
          <!-- About Us -->
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            关于我们
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown"> <!-- Dropdown option -->
              <a class="dropdown-item" href="about-us.php">简介</a>
              <a class="dropdown-item" href="#">图书馆出版刊物</a>
            </div>
          </li>

          <!-- OPAC -->
          <li class="nav-item">
            <a class="nav-link" href="#">查询书籍<span class="sr-only">(current)</span></a>
          </li>

          <!-- Research -->
          <li class="nav-item">
            <a class="nav-link" href="#">研究</a>
          </li>

          <!-- Resources -->
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            电子资源
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown"> <!-- Dropdown option -->
              <a class="dropdown-item" href="onlineDatabase.php">网络资源库 </a>
              <a class="dropdown-item" href="onlineJournal.php">网络期刊</a>
              <a class="dropdown-item" href="ebook.php">电子书</a>
              <a class="dropdown-item" href="#">多媒体资源</a>
              <a class="dropdown-item" href="enewspapers.php">旧报纸</a>
            </div>
          </li>

          <!-- Events -->
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            推广活动
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown"> <!-- Dropdown option -->
              <a class="dropdown-item" href="#">文化活动 </a>
              <a class="dropdown-item" href="#">展览  </a>
              <a class="dropdown-item" href="#">比赛  </a>
              <a class="dropdown-item" href="#">阅读活动</a>
              <a class="dropdown-item" href="#">讲座/工作坊   </a>
            </div>
          </li>

          <!-- Sponsorship -->
          <li class="nav-item">
            <a class="nav-link" href="#">赞助</a>
          </li>

          <!-- Careers -->
          <li class="nav-item">
            <a class="nav-link" href="#">人才招聘</a>
          </li>

          <!-- Membership -->
          <li class="nav-item">
            <a class="nav-link" href="membership.php">会员</a>
          </li>

          <!-- Contact Us -->
          <li class="nav-item">
            <a class="nav-link" href="contactUs.php">联系我们</a>
          </li>
        </ul>
        <div>
          <button id="en_click2" type="button" class="btn btn-danger">English</button>
          <h7 style="color: white; font-size: 20px;">/</h7>
          <button id="chi_click2" type="button" class="btn btn-danger mr-3">Chinese</button>
        </div>

        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Search</button>
        </form>
      </div>

    </nav>
