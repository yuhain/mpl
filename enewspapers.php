<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Anton">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <title>Malaysian Public Library</title>
  </head>
  <body class="news-bg">
    <?php include 'navigation.php'; ?>
    <div class="container-fluid en" style="height: 640px;">
    <h1 class="display-4 text-center mb-5 text-white ">Old E-Newspapers</h1>
    <br>
    <br>
    <br>
    <br>
    <h2 class="text-center display-3 mb-5 text-white">English E-Newspapers Coming Soon!</h2>
    <h3 class="text-center display-4 mb-5 text-white">Head over and click on Chinese up top to view the Chinese E-Newspapers!</h2>
    </div>
    <div class="container-fluid chi" style="height: 640px;">
      <h1 class="display-4 text-center mb-5 text-white ">旧报纸</h1>


      <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-interval="false">
        <ol class="carousel-indicators" style="bottom: -70px;">
          <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active">R-X</li>
          <li data-target="#carouselExampleCaptions" data-slide-to="1">Z</li>
        </ol>
        <div class="carousel-inner">
          <div class="container-sm">
            <div class="carousel-item active">
              <div class="row">
                <div class="col-md ml-1 mr-1" style="height: 450px;">
                  <div class="card bg-light" style="height: 348px;" data-aos="zoom-in">
                    <h5 class="card-header text-center text-body">R</h5>
                    <div class="card-body">

                      <div class="dropdown mb-3">
                        <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        日新报
                        </button>
                        <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                          <div class="col-md">
                            <hr><a class="dropdown-item" href="https://libportal.nus.edu.sg/frontend/ms/jit-shin-pau/index" target="_blank"><img src="Image\onlineDatabese\logo_2x.png" href="https://www.google.com.my/" class="rounded mx-auto d-block w-50" alt="..."></a><hr>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md ml-1 mr-1" style="height: 450px;">
                  <div class="card bg-light" style="height: 348px;" data-aos="zoom-in">
                    <h5 class="card-header text-center text-body">T</h5>
                    <div class="card-body">

                      <div class="dropdown mb-3">
                        <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        天南新报
                        </button>
                        <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                          <div class="col-md">
                            <hr><a class="dropdown-item" href="https://libportal.nus.edu.sg/frontend/ms/thien-nan-shin-pao/index" target="_blank"><img src="Image\onlineDatabese\tablet_banner_top.jpg" class="rounded mx-auto d-block w-100" alt="..."></a><hr>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md ml-1 mr-1" style="height: 450px;">
                  <div class="card bg-light" style="height: 348px;" data-aos="zoom-in">
                    <h5 class="card-header text-center text-body">X</h5>
                    <div class="card-body">

                        <div class="dropdown mb-3">
                          <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          香港旧报纸
                          </button>
                          <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                            <div class="col-md">
                              <hr><a class="dropdown-item" href="https://mmis.hkpl.gov.hk/old-hk-collection" target="_blank"><img src="Image\onlineDatabese\cern-logo-large.png" class="rounded mx-auto d-block w-25" alt="..."></a><hr>
                            </div>
                          </div>
                        </div>

                        <div class="dropdown mb-3">
                          <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          星报
                          </button>
                          <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                            <div class="col-md">
                              <hr><a class="dropdown-item" href="https://libportal.nus.edu.sg/frontend/ms/sing-po/index" target="_blank"><img src="Image\onlineDatabese\cern-logo-large.png" class="rounded mx-auto d-block w-25" alt="..."></a><hr>
                            </div>
                          </div>
                        </div>

                        <div class="dropdown mb-3">
                          <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          新国民日报
                          </button>
                          <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                            <div class="col-md">
                              <hr><a class="dropdown-item" href="https://libportal.nus.edu.sg/frontend/ms/sin-kuo-min/index" target="_blank"><img src="Image\onlineDatabese\cern-logo-large.png" class="rounded mx-auto d-block w-25" alt="..."></a><hr>
                            </div>
                          </div>
                        </div>

                        <div class="dropdown mb-3">
                          <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          星洲晨报
                          </button>
                          <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                            <div class="col-md">
                              <hr><a class="dropdown-item" href="https://libportal.nus.edu.sg/frontend/ms/sun-poo/index" target="_blank"><img src="Image\onlineDatabese\cern-logo-large.png" class="rounded mx-auto d-block w-25" alt="..."></a><hr>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <!--here -->
            <div class="carousel-item">
              <div class="row">
                <div class="col-md ml-1 mr-1" style="height: 450px;">
                  <div class="card bg-light" style="height: 348px;" data-aos="zoom-in">
                    <h5 class="card-header text-center text-body">Z</h5>
                    <div class="card-body">

                      <div class="dropdown mb-3">
                        <button class="btn btn-outline-secondary dropdown-toggle w-100 text-wrap" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        日新报
                        </button>
                        <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                          <div class="col-md">
                            <hr><a class="dropdown-item" href="https://libportal.nus.edu.sg/frontend/ms/jit-shin-pau/index" target="_blank"><img src="Image\onlineDatabese\logo_2x.png" href="https://www.google.com.my/" class="rounded mx-auto d-block w-50" alt="..."></a><hr>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> 
              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>

    <?php include 'footer.php'; ?>





    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
     AOS.init({
        duration: 2000
      });
    </script>
    <script src="js/language.js"></script>
  </body>
</html>
