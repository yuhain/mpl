<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Anton">
    <link rel="stylesheet" href ="css/language.css" type="text/css">
    <title>Malaysian Public Library</title>
  </head>
  <body class="lead text-center text-wrap">
    <?php include 'navigation.php'; ?>
    
    <!-- Membership Registration -->
    <div class="en">
      <h1 class="display-4 text-center mb-5 mt-5">Membership Registration</h1>
    </div>
    <div class="container en">
      <h6>For membership registration, please provide:</h6>

      <ul class="text-left en">
        <li>Photocopy of IC (MYCARD) (double sided) or photocopy of birth certificate (for those below   
            12 years of age)</li>
        <li>Photocopy of latest water bill or telephone bill (for verification of correspondence address)</li>
        <li>2 copies of passport size photo</li>
      </ul>
    </div>
    
    <div class="chi">
      <h1 class="display-4 text-center mb-5 mt-5">申请手续</h1>
    </div>
    <div class="container text-center chi ">
    <thead>For membership registration, please provide:</thead>
      <div class=" row justify-content-center">
      <table class="text-left">
        <tbody>
          <tr>
            <td>身份证(MYCARD)复印件 (双面)或 报生纸复印件 (12岁以下)</td>
            <td>1份</td>
          </tr>
          <tr>
            <td>住宅水费单或电话单复印件</td>
            <td>1份</td>
          </tr>
          <tr>
            <td>护照型相片</td>
            <td>2张</td>
          </tr>
        </tbody>
      </table>
      </div>
      

     
    </div>
<br>
<!-- Subscription Rates -->
<hr>
    <div class="en">
      <h1 class="display-4 text-center mb-5 mt-5">Subscription Rates</h1>
    </div>
  <div class="en">
  <div class="row w-100 ">
  <div class="col-sm ml-5">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title" style="text-align: center;">Student</h5>
        <p class="card-text">These are our rates for students.</p>
        <table class="table table-sm table-dark">
          <tbody>
            
      <tr>
        <th scope="row">Registration Fee</th>
        <td>RM 2.00</td>
      </tr>
      <tr>
        <th scope="row">Monthly</th>
        <td >RM 3.00</td>
      </tr>
      <tr class= "bg-secondary">
        <th scope="row">Total</th>
        <td>RM 3.00</td>
      </tr>
    </tbody>
        </table>
        <a href="#" class="btn btn-primary mx-auto d-block">Register Now!</a>
      </div>
    </div>
  </div>
   <div class="col-sm">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title" style="text-align: center;">Adult</h5>
        <p class="card-text">These are our rates for adults.</p>
        <table class="table table-sm table-dark">
          <tbody>
            
      <tr>
        <th scope="row">Registration Fee</th>
        <td>RM 5.00</td>
      </tr>
      <tr>
        <th scope="row">Monthly</th>
        <td >RM 5.00</td>
      </tr>
      <tr class= "bg-secondary">
        <th scope="row">Total</th>
        <td>RM 10.00</td>
      </tr>
    </tbody>
        </table>
        <a href="#" class="btn btn-primary mx-auto d-block">Register Now!</a>
      </div>
    </div>
  </div>
   <div class="col-sm">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title" style="text-align: center;">Retiree</h5>
        <p class="card-text">These are our rates for retirees.</p>
        <table class="table table-sm table-dark">
          <tbody>
            
      <tr>
        <th scope="row">Registration Fee</th>
        <td>RM 5.00</td>
      </tr>
      <tr>
        <th scope="row">Monthly</th>
        <td >RM 3.00</td>
      </tr>
      <tr class= "bg-secondary">
        <th scope="row">Total</th>
        <td>RM 8.00</td>
      </tr>
    </tbody>
        </table>
        <a href="#" class="btn btn-primary mx-auto d-block">Register Now!</a>
      </div>
    </div>
  </div>
  <div class="col-sm mr-5">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title" style="text-align: center;">OKU</h5>
        <p class="card-text">These are our rates for the disabled.</p>
        <table class="table table-sm table-dark">
          <tbody>
           
      <tr>
        <th scope="row">Registration Fee</th>
        <td>RM 5.00</td>
      </tr>
      <tr>
        <th scope="row">Monthly</th>
        <td >RM 3.00</td>
      </tr>
      <tr class= "bg-secondary">
        <th scope="row">Total</th>
        <td>RM 8.00</td>
      </tr>
    </tbody>
        </table>
        <a href="#" class="btn btn-primary mx-auto d-block">Register Now!</a>
      </div>
    </div>
  </div>
</div>
</div>
<!-- chinese subscription -->
<div class="chi">
      <h1 class="display-4 text-center mb-5 mt-5">收费</h1>
    </div>
<div class="chi">
  <div class="row w-100">
  <div class="col-sm ml-5">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title" style="text-align: center;">学生</h5>
        <p class="card-text">这些是我们给学生的价格。</p>
        <table class="table table-sm table-dark">
          <tbody>
            
      <tr>
        <th scope="row">首次注册</th>
        <td>RM 2.00</td>
      </tr>
      <tr>
        <th scope="row">每月</th>
        <td >RM 3.00</td>
      </tr>
      <tr class= "bg-secondary">
        <th scope="row">总费用</th>
        <td>RM 3.00</td>
      </tr>
    </tbody>
        </table>
        <a href="#" class="btn btn-primary mx-auto d-block">现在注册！</a>
      </div>
    </div>
  </div>
   <div class="col-sm">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title" style="text-align: center;">成人</h5>
        <p class="card-text">这些是我们的成人价格。</p>
        <table class="table table-sm table-dark">
          <tbody>
            
      <tr>
        <th scope="row">首次注册</th>
        <td>RM 5.00</td>
      </tr>
      <tr>
        <th scope="row">每月</th>
        <td >RM 5.00</td>
      </tr>
      <tr class= "bg-secondary">
        <th scope="row">总费用</th>
        <td>RM 10.00</td>
      </tr>
    </tbody>
        </table>
        <a href="#" class="btn btn-primary mx-auto d-block">现在注册！</a>
      </div>
    </div>
  </div>
   <div class="col-sm">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title" style="text-align: center;">退休人士</h5>
        <p class="card-text">这些是我们退休人员的费用。</p>
        <table class="table table-sm table-dark">
          <tbody>
            
      <tr>
        <th scope="row">首次注册</th>
        <td>RM 5.00</td>
      </tr>
      <tr>
        <th scope="row">每月</th>
        <td >RM 3.00</td>
      </tr>
      <tr class= "bg-secondary">
        <th scope="row">总费用</th>
        <td>RM 8.00</td>
      </tr>
    </tbody>
        </table>
        <a href="#" class="btn btn-primary mx-auto d-block">现在注册！</a>
      </div>
    </div>
  </div>
  <div class="col-sm mr-5">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title" style="text-align: center;">OKU</h5>
        <p class="card-text">这些是我们为残疾人士准备的价格。</p>
        <table class="table table-sm table-dark">
          <tbody>
           
      <tr>
        <th scope="row">首次注册</th>
        <td>RM 5.00</td>
      </tr>
      <tr>
        <th scope="row">每月</th>
        <td >RM 3.00</td>
      </tr>
      <tr class= "bg-secondary">
        <th scope="row">总费用</th>
        <td>RM 8.00</td>
      </tr>
    </tbody>
        </table>
        <a href="#" class="btn btn-primary mx-auto d-block">现在注册！</a>
      </div>
    </div>
  </div>
</div>
</div>
<br>
<br>
<hr>
<div class="rulesTitle">
  <h1 class="en" style = "text-align: center;
               margin-right: auto;
               margin-left: auto;
               padding-top: 50px;
               padding-bottom: 30px;
               font-family: Georgia;">Rules & Regulations</h1>
  <h1 class="chi" style = "text-align: center;
               margin-right: auto;
               margin-left: auto;
               padding-top: 50px;
               padding-bottom: 30px;
               font-family: Georgia;">规则</h1>
    

</div>
<div class="container en">
  
    <ul class="text-left text-wrap">
      <li>Outstation members/ non-Malaysians need to pay a deposit of RM60.00 (refundable with 
   official receipt)
      </li>
      <li>Loans are limited to 4 books each time
        <br>The loan period is 1 month; renewals are permitted. 
        <br>For overdue books, a charge of 10 sen per day per book is levied.
      </li>
      <li>For those below 12 years of age, the parent must be an active member.</li>
    </ul>  
  
</div>
<div class="container chi" >
 
    <ul>
      <li>外州会员或外国公民，都需另缴抵押金RM60.00，退会时，抵押金凭收据退还。</li>
      <li>每次限借4本, 为期1个月，逾期前可续借。逾期1天将罚款RM0.10一本，以此累积。</li>
      <li>12岁以下学生，父亲或母亲须先成为会员及有借书时，其小孩方可入会及借书。</li>
    </ul>  
 
</div>

</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<script src="js/language.js"></script>
<?php include 'footer.php'; ?>
</html>
